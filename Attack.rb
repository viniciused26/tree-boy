class Attack
  def initialize
    @attack_image = Gosu::Image.new("images_treeboy/apple.png")
    @attack_x = @attack_y = 0
  end

  def spawn_attack(x, y)
    @attack_x = x
    @attack_y = y
  end

  def draw_attack
    @attack_image.draw_rot(@attack_x, @attack_y, 1, 0.0)
  end
end
