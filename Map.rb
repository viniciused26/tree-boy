load 'Player.rb'
load 'Item.rb'

class Map
  def initialize
    @map_image = Gosu::Image.new("images_treeboy/default_map.png")
    @map_song = Gosu::Song.new("songs_treeboy/sunset_lullaby.mp3")
    @map_song.play

    @apple = Item.new
    @apple.spawn_item(370, 270)


  end

  def draw_map
    @map_image.draw_rot(350, 213, 1, 0.0)
    @apple.draw_item
  end
end
