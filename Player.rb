load 'Attack.rb'

class Player
  def initialize
    @player_image = Gosu::Image.new("images_treeboy/blob.png")
    @player_x = @player_y = 0
  end

  def spawn_player(x, y)
    @player_x = x
    @player_y = y
  end

  def player_move
    if Gosu.button_down? Gosu::KbA
      @player_x -= 3
    elsif Gosu.button_down? Gosu::KbD
      @player_x += 3
    elsif Gosu.button_down? Gosu::KbW
      @player_y -= 3
    elsif Gosu.button_down? Gosu::KbS
      @player_y += 3
    end
  end

  def player_attack
    if Gosu.button_down? Gosu::KbSpace
      @stone = Attack.new
      @stone.spawn_attack(@player_x, @player_y)
      @stone.draw_attack
    end
  end

  def draw_player
    @player_image.draw_rot(@player_x, @player_y, 1, 0.0)
  end
end
