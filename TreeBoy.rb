require 'gosu'
load 'Player.rb'
load 'Map.rb'
load 'Item.rb'

class TreeBoy < Gosu::Window
  def initialize
    super 700, 500
    self.caption = "Tree boy"

    @player = Player.new
    @player.spawn_player(350, 250)

    @map = Map.new
  end

  def update
    close if Gosu.button_down? Gosu::KbEscape
    @player.player_move
    @player.player_attack
  end

  def draw
    @map.draw_map
    @player.draw_player
  end
end

TreeBoy.new.show
