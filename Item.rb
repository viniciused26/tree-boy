class Item
  def initialize
    @image = Gosu::Image.new("images_treeboy/apple.png")
    @posicao_x
    @posicao_y
  end

  def spawn_item(x, y)
    @posicao_x = x
    @posicao_y = y
  end

  def draw_item
    @image.draw_rot(@posicao_x, @posicao_y, 1, 0.0)
  end
end

# if @player_inventory_itens[0] = "sto"
# stone = sto -> can be thrown at enemies to deal damage
# mushroom = mus -> increases movement speed for a short period
# apple = app -> recovers one heart
# cupcake = cup -> recovers all heart
# fairy = fai -> becomes immune to damage
# golden flower = gol -> increases the damage and the throw distance of the stone

#witch hat -> the player receive less damage from magic foes
#wand -> allows to deal damage to magic foes
#small backpack -> increases the inventory cappacity
#backpack -> increases the inventory cappacity
